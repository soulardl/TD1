<?php

////////////////////
// Initialisation //
////////////////////

use Symfony\Component\HttpFoundation\Request;
use TheFeed\Controleur\RouteurURL;

require_once __DIR__ . '/../vendor/autoload.php';


/////////////
// Routage //
/////////////

// Syntaxe alternative
// The null coalescing operator returns its first operand if it exists and is not null
RouteurURL::traiterRequete(Request::createFromGlobals())->send();


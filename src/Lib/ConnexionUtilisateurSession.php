<?php

namespace TheFeed\Lib;

use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\HTTP\Session;
use TheFeed\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateurSession implements ConnexionUtilisateurInterface
{
    private string $cleConnexion = "_utilisateurConnecte";

    // Note : Classe trop couplée avec les sessions
    public function connecter(string $idUtilisateur): void
    {
        $session = Session::getInstance();
        $session->enregistrer($this->cleConnexion, $idUtilisateur);
    }

    public function estConnecte(): bool
    {
        $session = Session::getInstance();
        return $session->existeCle($this->cleConnexion);
    }

    public function deconnecter()
    {
        $session = Session::getInstance();
        $session->supprimer($this->cleConnexion);
    }

    public function getIdUtilisateurConnecte(): ?string
    {
        $session = Session::getInstance();
        if ($session->existeCle($this->cleConnexion)) {
            return $session->lire($this->cleConnexion);
        } else
            return null;
    }
}

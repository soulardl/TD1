<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\FileMovingServiceInterface;
use TheFeed\Service\UtilisateurService;
use function PHPUnit\Framework\assertFileExists;

class UtilisateurServiceTest extends TestCase
{

    private $service;

    private $utilisateurRepositoryMock;

//Dossier où seront déplacés les fichiers pendant les tests
    private $dossierPhotoDeProfil = __DIR__ . "/tmp/";

    private FileMovingServiceInterface $fileMovingService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->fileMovingService = new TestFileMovingService();
        mkdir($this->dossierPhotoDeProfil);
        $this->service = new UtilisateurService($this->utilisateurRepositoryMock, $this->dossierPhotoDeProfil, $this->fileMovingService);
    }

    public function testCreerUtilisateurPhotoDeProfil()
    {
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn(null);
        $this->utilisateurRepositoryMock->method("recupererParEmail")->willReturn(null);
        $this->utilisateurRepositoryMock->method("ajouter")->willReturnCallback(function ($utilisateur) {
            assertFileExists($this->dossierPhotoDeProfil . $utilisateur->getNomPhotoDeProfil());

        });
        $this->service->creerUtilisateur("test", "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
    }

    public function testCreerUtilisateurPhotoDeProfilEmailFaux()
    {
        $donneesPhotoDeProfil = [];
        $this->expectException(ServiceException::class);
        $this->service->creerUtilisateur(null, "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
    }

    public function testRecuperutilisateurIdFaux()
    {
        $this->expectException(ServiceException::class);
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(null);
        $this->service->recupererUtilisateurParId(6, false);
    }

    public function testRecuperutilisateurId()
    {
        $ut = new Utilisateur();
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn($ut);
        $uti = $this->service->recupererUtilisateurParId(6);
        self::assertEquals($ut, $uti);
    }

    public function testDeconnecter(){
        $this->expectException(ServiceException::class);
        $this->service->deconnecterUtilisateur();
    }

    public function testDeconnecterd(){
        $this->expectException(ServiceException::class);
        $this->service->deconnecterUtilisateur();
    }

    protected function tearDown(): void
    {
//Nettoyage
        parent::tearDown();
        foreach (scandir($this->dossierPhotoDeProfil) as $file) {
            if ('.' === $file || '..' === $file) continue;
            unlink($this->dossierPhotoDeProfil . $file);
        }
        rmdir($this->dossierPhotoDeProfil);
    }

}
<?php

namespace TheFeed\Test;

use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\UtilisateurService;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

class PublicationServiceTest extends TestCase
{

    private PublicationService $publicationService;
    private UtilisateurService $utilisateurService;
    private PublicationRepositoryInterface $publicationRepositoryMock;
    private UtilisateurRepositoryInterface $utilisateurRepositoryMock;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepositoryInterface::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->publicationService = new PublicationService($this->publicationRepositoryMock, $this->utilisateurRepositoryMock);
        $this->utilisateurService = new UtilisateurService($this->utilisateurRepositoryMock);
    }

    public function testCreerPublicationUtilisateurInexistant(){
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(null);
        $utilisateur = $this->utilisateurService->recupererUtilisateurParId(-1);
        $this->publicationService->creerPublication($utilisateur, "test");
    }

    public function testCreerPublicationVide(){
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas être vide!");
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());
        $utilisateur = $this->utilisateurService->recupererUtilisateurParId(6);
        $this->publicationService->creerPublication($utilisateur, "");
    }

    public function testCreerPublicationTropGrande(){
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());
        $utilisateur = $this->utilisateurService->recupererUtilisateurParId(6);
        $this->publicationService->creerPublication($utilisateur, str_repeat("o", 251) );
    }

    /**
     * @throws \Exception
     */
    public function testNombrePublications(){
        //Fausses publications, vides
        $fakePublications = [new Publication(), new Publication()];
        //On configure notre faux repository pour qu'il renvoie nos publications définies ci-dessus
        $this->publicationRepositoryMock->method("recuperer")->willReturn($fakePublications);
        //Test
        $this->assertCount(2, $this->publicationService->recupererPublications());
    }

    /**
     * @throws \Exception
     */
    public function testNombrePublicationsUtilisateur(){
        $this->publicationRepositoryMock->method("recupererParAuteur")->willReturn([new Publication(), new Publication(), new Publication()]);
        assertCount(3, $this->publicationService->recupererPublicationsUtilisateur(4));
    }

    /**
     * @throws \Exception
     */
    public function testNombrePublicationsUtilisateurInexistant(){
        $this->publicationRepositoryMock->method("recupererParAuteur")->willReturn([]);
        assertCount(0, $this->publicationService->recupererPublicationsUtilisateur(-4));
    }

    /**
     * @throws ServiceException
     * @throws \Exception
     */
    public function testCreerPublicationValide(){
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(Utilisateur::create("tetete", "Mdzdzd500", "ezfz@zfoihfz.com", "anonyme.jpg"));
        $this->publicationRepositoryMock->method("ajouter")->willReturnCallback(function ($pub){
            $this->publicationRepositoryMock->method("recuperer")->willReturn([$pub]);
        });
        $this->publicationService->creerPublication(1, "test");
        assertCount(1, $this->publicationService->recupererPublications());
    }

}
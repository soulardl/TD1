<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateurInterface;
use TheFeed\Lib\ConnexionUtilisateurSession;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\Exception\ServiceException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ControleurPublicationAPI extends ControleurGenerique
{

    public function __construct (
        ContainerInterface $container,
        private readonly PublicationServiceInterface $publicationService,
        private ConnexionUtilisateurInterface $connexionUtilisateur
    )
    {
        parent::__construct($container);
    }

    #[Route(path: '/api/publications/{idPublication}', name:'supprimerPublicationAPI', methods:["DELETE"])]
    public function supprimer($idPublication): Response
    {
        try {
            $idUtilisateurConnecte = $this->connexionUtilisateur->getIdUtilisateurConnecte();
            $this->publicationService->supprimerPublication($idPublication, $idUtilisateurConnecte);
            return new JsonResponse('', Response::HTTP_OK);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }

    #[Route(path: '/api/publications/{idPublication}', name:'afficherDetailPublicationAPI', methods:["GET"])]
    public function afficherDetail($idPublication): Response{
        try {
            $publication = $this->publicationService->recupererPublicationParId($idPublication, false);
            return new JsonResponse($publication, Response::HTTP_OK);
        }catch (ServiceException $e){
            return new JsonResponse(["error" => $e->getMessage()], $e->getCode());
        }
    }

    #[Route(path: '/api/publications', name:'afficherListePublicationAPI', methods:["GET"])]
    public function afficherListe(): Response{
        try {
            $publications = $this->publicationService->recupererPublications();
            return new JsonResponse($publications, Response::HTTP_OK);
        }catch (ServiceException $e){
            return new JsonResponse(["error" => $e->getMessage()], $e->getCode());
        }
    }

    #[Route(path: '/api/publications', name:'posterPublicationAPI', methods:["POST"])]
    public function posterPublication(Request $request): Response
    {
        try {
            $corps = json_decode($request->getContent());
            $message = $corps->message ?? null;
            $idUtilisateurConnecte = $this->connexionUtilisateur->getIdUtilisateurConnecte();
            $publication = $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
            return new JsonResponse($publication, Response::HTTP_OK);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }catch (JsonException $exception) {
            return new JsonResponse(
                ["error" => "Corps de la requête mal formé"],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

}

<?php

namespace TheFeed\Controleur;

use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateurInterface;
use TheFeed\Lib\ConnexionUtilisateurSession;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurUtilisateur extends ControleurGenerique
{


    public function __construct(ContainerInterface $container,
            private PublicationServiceInterface $publicationService,
            private UtilisateurServiceInterface $utilisateurService,
            private readonly ConnexionUtilisateurInterface $connexionUtilisateurSession,
            private readonly ConnexionUtilisateurInterface $connexionUtilisateurJWT)
    {
        parent::__construct($container);
    }

    public function afficherErreur($messageErreur = "", $statusCode = ""): Response
    {
        return $this->afficherErreur($messageErreur, "utilisateur");
    }

    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublication', methods:["GET"])]
    public function afficherPublications($idUtilisateur): Response
    {
        try {
            $utilisateur = $this->utilisateurService->recupererUtilisateurParId($idUtilisateur, false);
            $loginHTML = htmlspecialchars($utilisateur->getLogin());
            $publications = $this->publicationService->recupererPublicationsUtilisateur($idUtilisateur);
            return self::afficherTwig("publication/page_perso.html.twig",["publications" => $publications, "pagetitle" => "Page perso de $loginHTML"] );

        }catch (ServiceException $e){
            MessageFlash::ajouter("error",$e);
            return $this->rediriger("afficherListe");
        }
    }

    #[Route(path: '/inscription', name:'inscription', methods:["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig("utilisateur/inscription.html.twig",
            [
                "method" => Configuration::getDebug() ? "get" : "post",
            ]

        );
    }

    #[Route(path: '/inscription', name:'afficherDepuisFormulaire', methods:["POST"])]
    public function creerDepuisFormulaire(): Response
    {
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;
        $adresseMail = $_POST['email'] ?? null;
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;

        try {
            $this->utilisateurService->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
        }catch (ServiceException $e){
            MessageFlash::ajouter("error", $e);
            return $this->rediriger("inscription");
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
       return $this->afficherTwig("utilisateur/connexion.html.twig");

    }

    #[Route(path: '/connexion', name:'connecter', methods:["POST"])]
    public function connecter(): Response
    {
        $login = $_POST["login"];
        $motDePasse = $_POST["mot-de-passe"];

        try {
            $idutilisateur = $this->utilisateurService->verifierIdentifiantUtilisateur($login, $motDePasse);
            $this->connexionUtilisateurSession->connecter($idutilisateur);
            $this->connexionUtilisateurJWT->connecter($idutilisateur);
        }catch (ServiceException $e){
            MessageFlash::ajouter("error", $e);
            return $this->rediriger("afficherFormulaireConnexion");
        }
        MessageFlash::ajouter("success", "Connexion effectuée.");
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/deconnexion', name:'deconnecter', methods:["GET"])]
    public function deconnecter(): Response
    {
        if (!$this->connexionUtilisateurSession->estConnecte()) {
            MessageFlash::ajouter("error", "Utilisateur non connecté.");
            return $this->rediriger('afficherListe');
        }
        $this->connexionUtilisateurSession->deconnecter();
        $this->connexionUtilisateurJWT->deconnecter();
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return $this->rediriger('afficherListe');
    }
}

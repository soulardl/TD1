<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateurInterface;
use TheFeed\Lib\ConnexionUtilisateurSession;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurPublication extends ControleurGenerique
{

    public function __construct(ContainerInterface $container,private PublicationServiceInterface $publicationService, private ConnexionUtilisateurInterface $connexionUtilisateur)
    {
        parent::__construct($container);
    }

    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    #[Route(path: '/', name:'web')]
    public function afficherListe(): Response
    {
        $publications = $this->publicationService->recupererPublications();
        return $this->afficherTwig("publication/feed.html.twig", ["publications" => $publications]);
    }

    #[Route(path: '/publications', name:'creerDepuisFormulaire', methods:["POST"])]
    public function creerDepuisFormulaire() : Response
    {
        $idUtilisateurConnecte = $this->connexionUtilisateur->getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try{
            $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
        }catch (ServiceException $e){
            MessageFlash::ajouter("error",$e);
        }

       return $this->rediriger( 'afficherListe');
    }




}
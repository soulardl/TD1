<?php

namespace TheFeed\Controleur;

 use Exception;
 use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\Response;
 use Symfony\Component\HttpFoundation\UrlHelper;
 use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
 use Symfony\Component\Routing\Exception\MethodNotAllowedException;
 use Symfony\Component\Routing\Exception\ResourceNotFoundException;
 use Symfony\Component\Routing\RequestContext;
 use Symfony\Component\Routing\Matcher\UrlMatcher;
 use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
 use Symfony\Component\HttpFoundation\RequestStack;
 use Symfony\Component\Routing\Generator\UrlGenerator;
 use TheFeed\Lib\ConnexionUtilisateurSession;
 use TheFeed\Lib\Conteneur;
 use TheFeed\Lib\MessageFlash;
 use Twig\Environment;
 use Twig\Loader\FilesystemLoader;
 use Twig\TwigFunction;
 use Symfony\Component\Config\FileLocator;
 use Symfony\Component\Routing\Loader\AttributeDirectoryLoader;
 use TheFeed\Lib\AttributeRouteControllerLoader;
 use Symfony\Component\DependencyInjection\ContainerBuilder;


 class RouteurURL
{
     /**
      * @throws Exception
      */
     public static function traiterRequete(Request $requete): Response
     {

        $conteneur = new ContainerBuilder();
        $conteneur->set('container', $conteneur);

        //On indique au FileLocator de chercher à partir du dossier de configuration
        $loader = new YamlFileLoader($conteneur, new FileLocator(__DIR__."/../Configuration"));
        //On remplit le conteneur avec les données fournies dans le fichier de configuration
        $loader->load("conteneur.yml");
        $conteneur->setParameter('project_root', __DIR__.'/../..');

        $twig=$conteneur->get('twig');

        $conteneur->set('request_context', (new RequestContext())->fromRequest($requete));

        $fileLocator = new FileLocator(__DIR__);
        $attrClassLoader = new AttributeRouteControllerLoader();

        $routes = (new AttributeDirectoryLoader($fileLocator, $attrClassLoader))->load(__DIR__);
        $conteneur->set('routes', $routes);

        $contexteRequete = $conteneur->get('request_context');
        $generateurUrl = $conteneur->get('url_generator');
        $assistantUrl = $conteneur->get('url_helper');

        $twig->addFunction(new TwigFunction("route", $generateurUrl->generate(...)));
        $twig->addFunction(new TwigFunction("asset", $assistantUrl->getAbsoluteUrl(...)));
        $twig->addGlobal('connexionUtilisateur', new ConnexionUtilisateurSession());
        $twig->addGlobal('messageFlash', new MessageFlash());

        /**
         ResourceNotFoundException If the resource could not be found
         MethodNotAllowedException If the resource was found but the request method is not allowed
         BadRequestException when the request has attribute "_check_controller_is_allowed" set to true and the controller is not allowed
         NoConfigurationException  If no routing configuration could be found
        \RuntimeException When no value could be provided for a required argument
         */

        try {
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ContainerControllerResolver($conteneur);
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $reponse = call_user_func_array($controleur, $arguments);
        } catch (ResourceNotFoundException $exception) {
            $reponse = $conteneur->get('controleur_generique')->afficherErreur($exception->getMessage(), 404);
        } catch (MethodNotAllowedException $exception) {
            $reponse = $conteneur->get('controleur_generique')->afficherErreur($exception->getMessage(), 405);
        } catch (Exception $exception) {
            $reponse = $conteneur->get('controleur_generique')->afficherErreur($exception->getMessage()) ;
        }

       return $reponse;
    }

}
<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateurInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurUtilisateurAPI extends ControleurGenerique
{


    public function __construct(private UtilisateurServiceInterface $service, private ContainerInterface $container, private ConnexionUtilisateurInterface $connexionUtilisateur)
    {
        parent::__construct($this->container);
    }

    #[Route(path: '/api/utilisateurs/{idUtilisateur}', name:'afficherDetail', methods:["GET"])]
    public function afficherDetail($idUtilisateur): Response{
        try {
            $utilisateur = $this->service->recupererUtilisateurParId($idUtilisateur, false);
            return new JsonResponse($utilisateur, Response::HTTP_OK);
        }catch (ServiceException $e){
            return new JsonResponse(["error" => $e->getMessage()], $e->getCode());
        }
    }

    #[Route(path: '/api/auth', name:'api_auth', methods:["POST"])]
    public function connecter(Request $request): Response
    {
        try {
            $jsonObject = json_decode($request->getContent(), flags: JSON_THROW_ON_ERROR);
            $login = $jsonObject->login ?? null;
            $password = $jsonObject->password ?? null;
            $idUtilisateur = $this->service->verifierIdentifiantUtilisateur($login, $password);
            $this->connexionUtilisateur->connecter($idUtilisateur);
            return new JsonResponse();
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        } catch (\JsonException $exception) {
            return new JsonResponse(
                ["error" => "Corps de la requête mal formé"],
                Response::HTTP_BAD_REQUEST
            );
        }
    }





}
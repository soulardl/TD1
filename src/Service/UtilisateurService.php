<?php

namespace TheFeed\Service;

use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateurSession;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{
    public function __construct(private UtilisateurRepositoryInterface $utilisateurRepository, private string $dossierPhotoDeProfil, private FileMovingServiceInterface $movingService){
    }


    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil) : void{
        if ($login != null || $motDePasse != null || $adresseMail != null || $nomPhotoDeProfil != null){
            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");
            }
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException("Mot de passe invalide!");
            }
            if (!filter_var($adresseMail, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException("L'adresse mail est incorrecte!");
            }

            $utilisateurRepository = $this->utilisateurRepository;
            $utilisateur = $utilisateurRepository->recupererParLogin($login);
            if ($utilisateur != null) {
                throw new ServiceException("Ce login est déjà pris!");
            }

            $utilisateur = $utilisateurRepository->recupererParEmail($adresseMail);
            if ($utilisateur != null) {
                throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");
            }

            $mdpHache = MotDePasse::hacher($motDePasse);

            // Upload des photos de profil
            // Plus d'informations :
            // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

            // On récupère l'extension du fichier
            $explosion = explode('.', $nomPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException("La photo de profil n'est pas au bon format!");
            }
            // La photo de profil sera enregistrée avec un nom de fichier aléatoire
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $nomPhotoDeProfil['tmp_name'];
            $to = $this->dossierPhotoDeProfil . "/$pictureName";
            $this->movingService->moveFile($from, $to);

            $utilisateur = Utilisateur::create($login, $mdpHache, $adresseMail, $pictureName);
            $utilisateurRepository->ajouter($utilisateur);

        } else {
            throw new ServiceException("Login, adresse mail, photo de profile ou mot de passe manquant.");
        }
    }

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId(string $idUtilisateur, bool $autoriserNull = true) : ?Utilisateur{
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);
        if (!$autoriserNull && $utilisateur == null){
            throw new ServiceException("L'utilisateur n'existe pas !", Response::HTTP_NOT_FOUND);
        }
        return $utilisateur;
    }

    /**
     * @throws ServiceException
     */
    public function verifierIdentifiantUtilisateur($login, $motDePasse) : int{
        if ($login==null || $motDePasse == null) {
            throw new ServiceException("Login ou mot de passe manquant", Response::HTTP_NOT_FOUND);
        }
        $utilisateur= $this->utilisateurRepository->recupererParLogin($login);
        if ($utilisateur == null) {
            throw new ServiceException("Login inconnu", Response::HTTP_NOT_FOUND);
        }
        if (!MotDePasse::verifier($motDePasse, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect", Response::HTTP_FORBIDDEN);
        }
        return $utilisateur->getIdUtilisateur();
    }





}
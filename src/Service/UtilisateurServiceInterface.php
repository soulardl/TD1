<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Service\Exception\ServiceException;

interface UtilisateurServiceInterface
{
    /**
     * @throws ServiceException
     */
    public function creerUtilisateur(string $login, string $motDePasse, string $adresseMail, array $nomPhotoDeProfil): void;

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId(string $idUtilisateur, bool $autoriserNull = true): ?Utilisateur;

    /**
     * @throws ServiceException
     */
    public function verifierIdentifiantUtilisateur(string $login, string $motDePasse): int;
}
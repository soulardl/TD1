<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Publication;
use TheFeed\Service\Exception\ServiceException;

interface PublicationServiceInterface
{
    public function recupererPublications(): array;

    /**
     * @throws ServiceException
     */
    public function creerPublication($idUtilisateur, $message): Publication;

    /**
     * @throws \Exception
     */
    public function recupererPublicationsUtilisateur($idUtilisateur): array;

    public function supprimerPublication(int $idPublication, ?string $idUtilisateurConnecte): void;

    public function recupererPublicationParId($idPublication, $autoriserNull = true) : ?Publication;
}